package com.orfiel.springmvcrestzadania;
import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.tags.Param;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Controller
public class controlerDemo {


    @GetMapping({"/","/nowe-mapowanie"})
    public String getDemo(){
        return "index.html";
    }

    @GetMapping("/find")
    @ResponseBody
    public String find(){
        return "Witaj";
    }

    @GetMapping("/find/{paramName}")
    @ResponseBody
    public String findithParam(@PathVariable("paramName") String paramName){
        return "Witaj" + paramName;
    }

    @GetMapping("/findWithQuery")
    @ResponseBody
    public String findithQuery(@RequestParam("paramName") String paramName){
        return "Witaj" + paramName;
    }


//    @GetMapping("/homes")
//    public String testRedirectWithSession(@RequestParam(value="redirect", required = false) String redirect,
//                                          HttpServletRequest request){
//        final HttpSession session = request.getSession();
//        if(redirect != null){
//            session.setAttribute("redirect", redirect);
//            return "redirect:"+redirect;
//        }
//        final String redirectFromSession = (String) session.getAttribute("redirect");
//        if(redirectFromSession == null){
//            return "redirect:404.html";
//        }
//        return "redirect:" + redirectFromSession;
//    }



//    @GetMapping("/homes")
//    public void testRedirectWithSession(@RequestParam(value="redirect", required = false) String redirect,
//                                          HttpServletResponse response) throws IOException {
//
//        response.sendRedirect("interia.pl");
//        return;
//
//    }
//
//
//    @RequestMapping
//    public String index(Map<String, Object> model){
//        model.put("name", "message");
//        return "dynamic";
//
//
//}

    public String listMultipleParams(HttpServletRequest request, Map<String, Object> model) throws IOException {
        final Map<String, String[]> parameterMap = request.getParameterMap();
        List<Param> params = new ArrayList<>();
        model.put("paramsFromReq", params);

        final Set<Map.Entry<String, String[]>> entries = parameterMap.entrySet();
        for(Map.Entry<String, String[]> entry : entries){
            Param p = new Param();
            p.key = entry.getKey();
            p.value = entry.getValue()[0];
            params.add(p);
        }
        return "dynamic";
    }

    public static class Param{
        private String key;
        private String value;
    }






















}
