package com.orfiel.springmvcrestzadania;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringMvcRestZadaniaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringMvcRestZadaniaApplication.class, args);
	}

}

